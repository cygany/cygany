* Download setup.ini and installer exe only if they differ (use some http checksum header)
* Functionality to debianize already installed cygwin packages *without reinstall*
* Functionality to make debian packages visible as cygwin packages for the cygwin *without reinstall*
* Check & sync functionality: find packages existing only in the debian or cygwin lists and offer fixes for them
* Functionality to download and install a prune cygwin, without any GUI interaction
* Manipulate cygwin packages without GUI interaction (?)
* Builder script to download and bootstrap-recompile all of our packages from a prune cygwin (♥ gentoo ♥)
    Note:
        - some packages like dh-autoreconf, builds and compiles well by us
        - more packages in projects/cygany are not actually used (buffer, alien, dwz (?), shunit2)
        - some projects have an upstream git repo, others do not
* Debianize this whole thingy, it must be available also as a deb package
* Top prio package to migrate from debian: deborphan, apt-file, dpkg-repack
